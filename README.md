checkbox-flag
=============

A jQuery Plugin to wrap an ordinary input[type=checkbox] with magic to draw attention to itself and disable other elements until it has been checked


## License
&copy; 2012 [Daniel J. Post](http://danieljpost.info)

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.
