/**
 * @author Daniel J. Post <danieljpost@gmail.com>
 * @version .9.1
 * @TODO: "use strict"
 * @TODO: enable overriding 'flag-on','flag-off' CSS classNames
 * @TODO: try to minify
 * @TODO: add github url & steal other best practices from better plugins
 */
/*
 * usage:
 * 
 * $('#example0 :checkbox.flag').addFlag();
 * 
 * $('#example1 :checkbox.flag').acceptTerms();
 * 
 * $('#example2:checkbox.flag').acceptTerms(
 * 
 * {linkOpen: function () { alert('Custom linkOpen');return false; } } );
 * 
 * $('#example3 :checkbox.flag').acceptTerms();
 * 
 * $('#consentBox5').addFlag( {on: function () {$().styleFlagOn('#consentBox5');
 * alert('ON');}, off: function () {$().styleFlagOff('#consentBox5');
 * alert('OFF');}, clearCheckbox:false });
 * 
 */
(function ($) {
    // "use strict";

    /**
     * These are config options for the plugin
     */
    $.checkboxFlag = {
        // default settings
        cbFDefaults : {
            // boolean - should checkbox be deliberately disabled
            disableCheckBox : false,
            // boolean - should checkbox be deliberately cleared
            clearCheckbox : true,
            // boolean - should user click related links
            mustReadTerms : false,
            linkOpen : null,
            // function - called when checkbox is clicked "on"
            on : this.styleFlagOn,
            // function - called when checkbox is clicked "off"
            off : this.styleFlagOff
        },
        // override default settings for "accept terms" links
        cbFAccept : {
            disableCheckBox : true,
            mustReadTerms : true,
            linkOpen : function (a) {
                window.open(
                    // link href
                    $(a).attr('href'),
                    // a "unique" window ID
                    $.checkboxFlag.winUUId += 1,
                    // window parameters
                    "location=1,status=0,scrollbars=1,width=300,height=400"
                );
                // stop browser from loading link in current window.
                return false;
            }
        },
        // "counter" guarantee that each a[role~=accept-this] opens in new window
        winUUId : 0
    };

    /*
     * This is the plugin
     */
    var options = {},
        checkboxFlag = {
            // adds "flag" next to checkbox.
            pvtaddFlag : function () {
                // skip if not a checkbox
                if (!$(this).is(':checkbox')) {
                    return false;
                }
                var dflag = $(this).attr('id') + '-flag';
                // skip if this "flag" exists
                if (!$('#' + dflag).length) {
                    // create div.flag.flag-default before the checkbox
                    $('<div>').addClass('flag').addClass('flag-default')
                        .attr('id', dflag).insertBefore(this);
                    // add "data" to checkbox
                    // $(this).attr('data-flag', dflag);
                    $(this).data('flag', dflag);
                }
            },
            makeItSo : function (userOptions) {
                // if nothing selected, return nothing; can't chain anyway
                // if not a checkbox, don't flag it
                if (!(this.length && $(this).is(':checkbox'))) {
                    "undefined" !== typeof console
                        && console.log("bad selector, can't add flags, returning nothing");
                    return;
                }
                // jquery optimization
                var $boxen = $(this),
                    options = $.extend({}, this.cbFDefaults, userOptions),
                    on = ('function' === typeof options.on) ? options.on
                        : this.styleFlagOn,
                    off = ('function' === typeof options.off) ? options.off
                        : this.styleFlagOff;
                $boxen
                    // create "flag" to left of each checkbox
                    .each(this.pvtaddFlag)
                    // if desired, force the links & make em come up in another window
                    .each(this.setLinkBehavior)
                    // if desired, find all "related" links and require they be clicked
                    .each(this.doRelatedLinks)
                    // *Always* clear checkbox by default.
                    .each(function () {
                        var $t = $(this),
                            d = $t.attr('disabled');
                            // enable
                        $t.attr('disabled', null)
                            // uncheck
                            .attr('checked', null)
                            // set back to previous disabled/enabled state
                            .attr('disabled', d);
                    })
                    // clear/set the checkbox if it's supposed to be done.
                    .each(this.uncheckCB)
                    // disable the checkbox if it's supposed to be done.
                    .each(this.disableCB)
                    // add click function to the checkbox.
                    .click(function () {
                        var me = $(this);
                        // Set up "on" and "off" functions.
                        this.checked ? on(this) : off(this);
                        // Add CSS in case this is useful
                        $(this).toggleClass('checked');
                    });
            },
            setLinkBehavior : function () {
                if (options.mustReadTerms) {
                    var func = options.linkOpen,
                        $boxId = $(this).attr('id'),
                        i = 0;
                    // find <a> related to $boxId
                    $('a[role~="accept-' + $boxId + '"]' + ', '
                            + ' a[role~="consent-' + $boxId + '"]')
                        // strip normal behavior (to open link in same window)
                        .unbind()
                        // apply specified function to link click
                        .click(function () {
                            return func($(this));
                        });
                }
            },
            // 
            doRelatedLinks : function () {
                if (options.mustReadTerms) {
                    var $boxId = $(this).attr('id');
                    $('a[role~="accept-' + $boxId + '"], a[role~="consent-'
                            + $boxId + '"]').click(function () {
                        // add "data" to link (mark it "read")
                        // $(this).attr('data-read', true);
                        $(this).data('read', true);
                        // ask checkbox if related links have been "read"
                        // enable it if all are "read"
                        // role could have been 'accept-foo accept-bar consent-baz'.
                        // isolate foo & bar && make selector #foo,#bar
                        var selector = '#' + $boxId;
                        $(selector).attr('disabled', !($(selector).termsAreRead()));
                    });
                }
            },
            // clears checkboxes under right conditions
            uncheckCB : function () {
                (options.clearCheckbox) ?
                        // clear
                        $(this).attr('checked', null).removeClass('checked') :
                        // set
                        $(this).attr('checked', 'checked').addClass('checked');
            },
            // disables checkboxes under right conditions
            disableCB : function () {
                $(this).attr('disabled', options.disableCheckBox);
            },

            /* "public" functions */
            // returns true if all requisite links have been clicked
            // $('#example2 :checkbox.flag').termsAreRead()
            termsAreRead : function () {
                // fail gracefully: if there are no links then none could be clicked
                var tAR = true;
                // find related links
                $('a[role~="accept-' + $(this).attr('id')
                    + '"], a[role~="consent-' + $(this).attr('id') + '"]')
                    // if any hasn't been clicked then termsAreRead() returns false
                    .each(function () {
/*
                        if (!$(this).attr('data-read')) {
                            tAR = false;
                        }
*/
                        if (!$(this).data('read')) {
                            tAR = false;
                        }
                    });
                return tAR;
            },
            // turns the "arrow" green (it is "yellow" by default)
            // $().styleFlagOn($('#acceptBox1'))
            styleFlagOn : function (obj) {
                $('#' + $(obj).attr('id') + '-flag')
                    .addClass('flag-on')
                    .removeClass('flag-default')
                    .removeClass('flag-off');
            },
            // turns the "arrow" red (it is "yellow" by default)
            // $().styleFlagOff($('#acceptBox1'))
            styleFlagOff : function (obj) {
                $('#' + $(obj).attr('id') + '-flag')
                    .addClass('flag-off')
                    .removeClass('flag-default')
                    .removeClass('flag-on');
            },
            // adds flag to specified checkbox
            // $('#example0 :checkbox.flag').addFlag();
            // var userOptions is defined above TODO: write API
            addFlag : function (userOptions) {
                userOptions = userOptions || {};
                options = $.extend({}, $.checkboxFlag.cbFDefaults, userOptions);
                this.makeItSo(options);
            },
            // adds flag to specified checkbox, plus
            // user must click related link to enable checkbox.
            // $('#example1 :checkbox.flag').acceptTerms();
            acceptTerms : function (userOptions) {
                userOptions = userOptions || {};
                options = $.extend({}, $.checkboxFlag.cbFAccept, userOptions);
                this.addFlag(options);
            }
        };

    // adds above to jQuery
    $.extend($.fn, checkboxFlag);

}(jQuery));
